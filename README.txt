================================================================================
DESCRIPTION
================================================================================

This is a Chip-8 emulator written as a learning exercise and following these
specifications:
http://devernay.free.fr/hacks/chip8/C8TECH10.HTM

If you don't have any Chip-8 roms to run, you can find some here:
http://www.zophar.net/pdroms/chip8/chip-8-games-pack.html
http://www.chip8.com/?page=109

You can view a video of this emulator in action here:
https://www.youtube.com/watch?v=yLOC6WbQfAA

================================================================================
USAGE
================================================================================

After launching the emulator, load a rom by navigating to File > Load, selecting
a Chip-8 rom, and then clicking the "Open" button.

The keyboard controls are as follows:
1 -> 1
2 -> 2
3 -> 3
4 -> 4
5 -> 5
6 -> 6
7 -> 7
8 -> 8
9 -> 9
A -> a
B -> b
C -> c
D -> d
E -> e
F -> f

The controls cannot be changed at this time. I realize that they aren't layed out
in a manner that makes them comfortable to use, but a comfortable control scheme
is really outside the scope of this project.