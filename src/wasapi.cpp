#include "wasapi.hpp"

#include "com_pointer.hpp"
#include <functional>
#include "windows_exception.hpp"

void wasapi::_initialize() {
    HRESULT hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);
    WINDOWS_EXCEPTION_IF_FAILED("COM intialization failed.", hr);

    IMMDeviceEnumerator* enumerator = nullptr;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator),
                          nullptr,
                          CLSCTX_ALL,
                          __uuidof(IMMDeviceEnumerator),
                          reinterpret_cast<void**>(&enumerator));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve multimedia device enumerator.", hr);
    _device_enumerator = make_com_pointer(enumerator);

    IMMDevice* default_device = nullptr;
    _device_enumerator->GetDefaultAudioEndpoint(eRender, eConsole, &default_device);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve default audio device.", hr);
    _default_device = make_com_pointer(default_device);

    IAudioClient* audio_client = nullptr;
    hr = _default_device->Activate(__uuidof(IAudioClient), CLSCTX_ALL, nullptr, reinterpret_cast<void**>(&audio_client));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve default device audio client.", hr);
    _dd_audio_client = make_com_pointer(audio_client);

    WAVEFORMATEX* wfp = nullptr;
    hr = _dd_audio_client->GetMixFormat(&wfp);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve wave format for default device.", hr);
    std::unique_ptr<WAVEFORMATEX, std::function<void(void*)>> waveformat(wfp, [](void* p) { CoTaskMemFree(p); });

    _samples_per_second = waveformat->nSamplesPerSec;
    _num_channels = waveformat->nChannels;

    hr = audio_client->Initialize(AUDCLNT_SHAREMODE_SHARED, 0, _reftimes_per_s, 0, waveformat.get(), nullptr);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to initialize default device.", hr);

    IAudioRenderClient* render_client = nullptr;
    hr = audio_client->GetService(__uuidof(IAudioRenderClient), reinterpret_cast<void**>(&render_client));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve audio render client for default device.", hr);
    _dd_render_client = make_com_pointer(render_client);
}

wasapi::wasapi(const REFERENCE_TIME reftimes_per_s)
    : _device_enumerator(nullptr)
    , _default_device(nullptr)
    , _dd_audio_client(nullptr)
    , _dd_render_client(nullptr)
    , _reftimes_per_s(reftimes_per_s)
    , _samples_per_second(0)
    , _num_channels(0)
{
    _initialize();
}

std::shared_ptr<IAudioClient> wasapi::get_dd_audio_client() {
    return _dd_audio_client;
}

std::shared_ptr<IAudioRenderClient> wasapi::get_dd_render_client() {
    return _dd_render_client;
}

uint64_t wasapi::get_samples_per_second() const {
    return _samples_per_second;
}

uint32_t wasapi::get_num_channels() const {
    return _num_channels;
}
