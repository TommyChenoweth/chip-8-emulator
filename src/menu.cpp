#include "menu.hpp"

#include "d3d_exception.hpp"
#include <spdlog/spdlog.h>

uint32_t menu::_next_id = 0;
std::unordered_map<uint32_t, menu_callback_t> menu::_callbacks;
std::unordered_map<std::wstring, uint32_t> menu::_menu_name_to_id;

menu::menu() 
    : _menu_handle(CreateMenu())
{
}

void menu::append_item(const std::wstring& name, const uint32_t flags) {
    _menu_name_to_id.insert({name, _next_id});
    AppendMenu(_menu_handle, flags, _next_id, name.c_str());
    ++_next_id;
}

void menu::append_item(const std::wstring& name, menu_callback_t callback, const uint32_t flags) {
    _callbacks.insert({ _next_id, callback });
    append_item(name, flags);
}

void menu::append_dropdown(const std::wstring& name, menu& dropdown) {
    HMENU menu_handle = dropdown.get_menu_handle();
    AppendMenu(_menu_handle, MF_POPUP | MF_STRING, reinterpret_cast<UINT_PTR>(menu_handle), name.c_str());
}

void menu::check_menu_item(const std::wstring& name) {
    const auto item_id = _menu_name_to_id[name];
    MENUITEMINFO info;
    memset(&info, 0, sizeof(info));
    info.cbSize = sizeof(MENUITEMINFO);
    info.fMask = MIIM_STATE;
    GetMenuItemInfo(_menu_handle, item_id, false, &info);
    info.fState |= MFS_CHECKED;
    SetMenuItemInfo(_menu_handle, item_id, false, &info);
}

void menu::uncheck_menu_item(const std::wstring& name) {
    const auto item_id = _menu_name_to_id[name];
    MENUITEMINFO info;
    memset(&info, 0, sizeof(info));
    info.cbSize = sizeof(MENUITEMINFO);
    info.fMask = MIIM_STATE;
    GetMenuItemInfo(_menu_handle, item_id, false, &info);
    info.fState &= ~MFS_CHECKED;
    SetMenuItemInfo(_menu_handle, item_id, false, &info);
}

HMENU menu::get_menu_handle() {
    return _menu_handle;
}

void menu::invoke_handler_for_menu_id(const uint32_t id) {
    auto func_iter = _callbacks.find(id);
    if(func_iter == _callbacks.end()) {
        spdlog::get(L"logger")->error(L"No callback for menu with id {}", id);
        return;
    }
    std::get<1>(*func_iter)();
}
