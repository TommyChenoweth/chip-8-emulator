#pragma once

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <Windows.h>

typedef std::function<void(void)> menu_callback_t;

class menu {
private:
    HMENU                                                _menu_handle;
    static uint32_t                                      _next_id;
    static std::unordered_map<uint32_t, menu_callback_t> _callbacks;
    static std::unordered_map<std::wstring, uint32_t>    _menu_name_to_id;

public:
    menu();

    void append_item(const std::wstring& name, const uint32_t flags = MF_STRING);
    void append_item(const std::wstring& name, menu_callback_t callback, const uint32_t flags = MF_STRING);
    void append_dropdown(const std::wstring& name, menu& dropdown);
    void check_menu_item(const std::wstring& name);
    void uncheck_menu_item(const std::wstring& name);

    HMENU get_menu_handle();

    static void invoke_handler_for_menu_id(const uint32_t id);
};