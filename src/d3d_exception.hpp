#pragma once

#include "windows_exception.hpp"

#include <d3d11.h>
#include <Windows.h>

class d3d_exception : public windows_exception {
private:
    std::string  _debug_messages;

    std::wstring _retrieve_dx_message();

public:
    explicit d3d_exception(const std::string& what_arg, const HRESULT hr);
    explicit d3d_exception(const char* what_arg, const HRESULT hr);
    explicit d3d_exception(const char* what_arg, const HRESULT hr, const wchar_t* function, const wchar_t* file, int line);
    explicit d3d_exception(const char* what_arg, const HRESULT hr, const wchar_t* function, const wchar_t* file, int line, ID3D11Device* device);

    const char* debug_messages() const;
};

#ifndef D3DEXCEPTION
#define D3DEXCEPTION(d, r) \
    d3d_exception(d, r, __LPREFIX(__FUNCTION__), WIDEN(__FILE__), __LINE__);
#endif
#ifndef D3DEXCEPTION_D
#define D3DEXCEPTION_D(d, r, i) \
    d3d_exception(d, r, __LPREFIX(__FUNCTION__), WIDEN(__FILE__), __LINE__, i);
#endif