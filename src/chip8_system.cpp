#include "chip8_system.hpp"

#include <spdlog/spdlog.h>

void chip8_system::_create_numeric_sprite_data() {
    char data[16][5] = {
        { 0xF0, 0x90, 0x90, 0x90, 0xF0 },
        { 0x20, 0x60, 0x20, 0x20, 0x70 },
        { 0xF0, 0x10, 0xF0, 0x80, 0xF0 },
        { 0xF0, 0x10, 0xF0, 0x10, 0xF0 },
        { 0x90, 0x90, 0xF0, 0x10, 0x10 },
        { 0xF0, 0x80, 0xF0, 0x10, 0xF0 },
        { 0xF0, 0x80, 0xF0, 0x90, 0xF0 },
        { 0xF0, 0x10, 0x20, 0x40, 0x40 },
        { 0xF0, 0x90, 0xF0, 0x90, 0xF0 },
        { 0xF0, 0x90, 0xF0, 0x10, 0xF0 },
        { 0xF0, 0x90, 0xF0, 0x90, 0x90 },
        { 0xE0, 0x90, 0xE0, 0x90, 0xE0 },
        { 0xF0, 0x80, 0x80, 0x80, 0xF0 },
        { 0xE0, 0x90, 0x90, 0x90, 0xE0 },
        { 0xF0, 0x80, 0xF0, 0x80, 0xF0 },
        { 0xF0, 0x80, 0xF0, 0x80, 0x80 },
    };

    uint16_t next_address = _numeric_addresses_start;
    for(size_t i = 0; i < _numeric_addresses.size(); ++i) {
        _ram->store(&data[i][0], _numeric_sprite_size_in_bytes, next_address);
        _numeric_addresses[i] = next_address;
        next_address += _numeric_sprite_size_in_bytes;
    }
}

void chip8_system::_interpret_instruction(uint16_t instruction) {
    spdlog::get(L"logger")->debug(L"Opcode {:X}\n"
                                  L"0:{}, 1:{}, 2:{}, 3:{}, 4:{}, 5:{}, 6:{}, 7:{}, 8:{}, 9:{}, A:{}, B:{}, C:{}, D:{}, E:{}, F:{}\n"
                                  L"PC:{:X}, I:{:X}, SP:{:X}",
                                  instruction,
                                  _V[0], _V[1], _V[2], _V[3], _V[4], _V[5], _V[6], _V[7], _V[8], _V[9], _V[10], _V[11], _V[12], _V[13], _V[14], _V[15],
                                  _PC, _I, _SPt);
    switch(instruction & 0xF000) {
    case 0x0000:
        switch(instruction) {
        case 0x00E0: // Clear the display.
            _display->clear();
            _display_is_dirty = true;
            break;
        case 0x00EE: // Return from a subroutine.
            _return_from_subroutine(instruction);
            break;
        default:
            spdlog::get(L"logger")->error(L"Ignoring jump to machine code routine: {:X}", instruction);
        }
        break;
    case 0x1000: // 1nnn - Jump to location nnn.
        _jump(instruction);
        break;
    case 0x2000: // 2nnn - Call subroutine at nnn.
        _call_subroutine(instruction);
        break;
    case 0x3000: // 3xkk - Skip next instruction if Vx = kk.
        _skip_if_vx_eq_kk(instruction);
        break;
    case 0x4000: // 4xkk - Skip next instruction if Vx != kk.
        _skip_if_vx_ne_kk(instruction);
        break;
    case 0x5000: // 5xy0 - Skip next instrruction if Vx = Vy.
        _skip_if_vx_eq_vy(instruction);
        break;
    case 0x6000: // 6xkk - Set Vx = kk.
        _set_vx_to_kk(instruction);
        break;
    case 0x7000: // 7xkk - Set Vx = Vx + kk.
        _set_vx_inc_by_kk(instruction);
        break;
    case 0x8000:
        switch(instruction & 0xF00F) {
        case 0x8000: // 8xy0 - Set Vx = Vy.
            _set_vx_to_vy(instruction);
            break;
        case 0x8001: // 8xy1 - Set Vx = Vx | Vy.
            _set_vx_to_vx_or_vy(instruction);
            break;
        case 0x8002: // 8xy2 - Set Vx = Vx & Vy.
            _set_vx_to_vx_and_vy(instruction);
            break;
        case 0x8003: // 8xy3 - Set Vx = Vx ^ Vy.
            _set_vx_to_vx_xor_vy(instruction);
            break;
        case 0x8004: // 8xy4 - Set Vx = Vx + Vy. VF = carry.
            _set_vx_to_vx_plus_vy(instruction);
            break;
        case 0x8005: // 8xy5 - Set Vx = Vx - Vy. VF = NOT borrow.
            _set_vx_to_vx_minus_vy(instruction);
            break;
        case 0x8006: // 8xy6 - Set VF = 0x1 & Vx. Vx = Vx >> 1.
            _set_vx_to_rshift_vx(instruction);
            break;
        case 0x8007: // 8xy7 -  VF = Vx < Vy ? 1 : 0. Vx = Vy - Vx.
            _set_vx_to_vy_minus_vx(instruction);
            break;
        case 0x800E: // 8xyE - VF = 0 < (Vx & 0x80) ? 1 : 0. Vx = Vx << 1.
            _set_vx_to_lshift_vx(instruction);
            break;
        default:
            spdlog::get(L"logger")->error(L"Unknown Opcode: {:X}", instruction);
        }
        break;
    case 0x9000: // 9xy0 - Skip the next instruction if Vx != Vy.
        _skip_if_vx_ne_vy(instruction);
        break;
    case 0xA000: // Annn - I = nnn.
        _set_i_to_address(instruction);
        break;
    case 0xB000: // Bnnn - PC = nnn + V0.
        _jump_offset_v0(instruction);
        break;
    case 0xC000: // Cxkk - Vx = Random Byte & kk.
        _set_vx_to_rand(instruction);
        break;
    case 0xD000: // Dxyn - Display n byte sprite at memory location I at (Vx, Vy). VF = collision.
        _draw_sprite_at_address(instruction);
        break;
    case 0xE000:
        switch(instruction & 0xF0FF) {
        case 0xE09E: // Ex9E - Skip next instruction if key with value Vx is pressed.
            _skip_if_vx_down(instruction);
            break;
        case 0xE0A1: // ExA1 - Skip next instruction if key with value Vx is not pressed.
            _skip_if_vx_up(instruction);
            break;
        default:
            spdlog::get(L"logger")->error(L"Unknown Opcode: {:X}", instruction);
        }
        break;
    case 0xF000:
        switch(instruction & 0xF0FF) {
        case 0xF007: // Fx07 - Vx = DT.
            _set_vx_to_dt(instruction);
            break;
        case 0xF00A: // Fx0A - Wait for keypress. Vx = value of keypress.
            _wait_for_vx_down(instruction);
            break;
        case 0xF015: // Fx15 - DT = Vx.
            _set_dt_to_vx(instruction);
            break;
        case 0xF018: // Fx18 - ST = Vx.
            _set_st_to_vx(instruction);
            break;
        case 0xF01E: // Fx1E - I = I + Vx.
            _set_i_inc_vx(instruction);
            break;
        case 0xF029: // Fx29 - I = Address of sprite for digit Vx.
            _set_i_numeric_sprite_address(instruction);
            break;
        case 0xF033: // Fx33 - BCD of Vx stored in locations I, I+1, I+2.
            _store_bcd_of_vx_at_i(instruction);
            break;
        case 0xF055: // Fx55 - Store registers V0 - Vx in memory starting at address I.
            _store_registers_at_i(instruction);
            break;
        case 0xF065: // Fx65 - Stores values starting at address I into registers V0 - Vx.
            _load_registers_from_i(instruction);
            break;
        default:
            spdlog::get(L"logger")->error(L"Unknown Opcode: {:X}", instruction);
        }
        break;
    default:
        spdlog::get(L"logger")->error(L"Unknown Opcode: {:X}", instruction);
    }
}

void chip8_system::_push_address_to_stack(const uint16_t address) {
    const uint16_t next_sp_address = _SPt + _address_size_in_bytes;
    _ram->store(&address, _address_size_in_bytes, next_sp_address);
    _SPt = next_sp_address;
}

int chip8_system::_pop_address_from_stack() {
    const auto address = *reinterpret_cast<uint16_t*>(_ram->load(_SPt, _address_size_in_bytes).get());
    _SPt -= _address_size_in_bytes;
    return address;
}

void chip8_system::_return_from_subroutine(const uint16_t instruction) {
    _PC = _pop_address_from_stack();
}

void chip8_system::_jump(const uint16_t instruction) {
    const uint16_t nnn = (instruction & 0x0FFF) >> 0;
    _PC = nnn;
}

void chip8_system::_call_subroutine(const uint16_t instruction) {
    const uint16_t nnn = (instruction & 0x0FFF) >> 0;
    _push_address_to_stack(_PC);
    _PC = nnn;
}

void chip8_system::_skip_if_vx_eq_kk(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t kk = (instruction & 0x00FF) >> 0;
    if(_V[x] == kk)
        _PC += _instruction_size_in_bytes;
}

void chip8_system::_skip_if_vx_ne_kk(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t kk = (instruction & 0x00FF) >> 0;
    if(_V[x] != kk)
        _PC += _instruction_size_in_bytes;
}

void chip8_system::_skip_if_vx_eq_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    if(_V[x] == _V[y])
        _PC += _instruction_size_in_bytes;
}

void chip8_system::_set_vx_to_kk(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t kk = (instruction & 0x00FF) >> 0;
    _V[x] = kk;
}

void chip8_system::_set_vx_inc_by_kk(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t kk = (instruction & 0x00FF) >> 0;
    _V[x] = _V[x] + kk;
}

void chip8_system::_set_vx_to_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    _V[x] = _V[y];
}

void chip8_system::_set_vx_to_vx_or_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    _V[x] = _V[x] | _V[y];
}

void chip8_system::_set_vx_to_vx_and_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    _V[x] = _V[x] & _V[y];
}

void chip8_system::_set_vx_to_vx_xor_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    _V[x] = _V[x] ^ _V[y];
}

void chip8_system::_set_vx_to_vx_plus_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    const unsigned sum = _V[x] + _V[y];
    _V[0xF] = 0xFF < sum ? 1 : 0;
    _V[x] = sum;
}

void chip8_system::_set_vx_to_vx_minus_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    _V[0xF] = _V[y] <= _V[x] ? 1 : 0;
    _V[x] = _V[x] - _V[y];
}

void chip8_system::_set_vx_to_rshift_vx(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _V[0xF] = _V[x] & 0x1 ? 1 : 0;
    _V[x] >>= 1;
}

void chip8_system::_set_vx_to_vy_minus_vx(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    _V[0xF] = _V[x] <= _V[y] ? 1 : 0;
    _V[x] =  _V[y] - _V[x];
}

void chip8_system::_set_vx_to_lshift_vx(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _V[0xF] = _V[x] & 0x80 ? 1 : 0;
    _V[x] <<= 1;
}

void chip8_system::_skip_if_vx_ne_vy(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    if(_V[x] != _V[y])
        _PC += _instruction_size_in_bytes;
}

void chip8_system::_set_i_to_address(const uint16_t instruction) {
    const uint16_t nnn = (instruction & 0x0FFF) >> 0;
    _I = nnn;
}

void chip8_system::_jump_offset_v0(const uint16_t instruction) {
    const uint16_t nnn = (instruction & 0x0FFF) >> 0;
    _PC = nnn + _V[0];
}

void chip8_system::_set_vx_to_rand(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t kk = (instruction & 0x00FF) >> 0;
    _V[x] = rand() & kk;
}

void chip8_system::_draw_sprite_at_address(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const uint8_t y = (instruction & 0x00F0) >> 4;
    const uint8_t size = (instruction & 0x000F) >> 0;
    const auto sprite = _ram->load(_I, size);
    const bool collision = _display->render_sprite(sprite, size, _V[x], _V[y]);
    _V[0xF] = collision ? 1 : 0;
    _display_is_dirty = true;
}

void chip8_system::_skip_if_vx_down(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const c8key key = static_cast<c8key>(_V[x]);
    if(_input->is_key_down(key))
        _PC += _instruction_size_in_bytes;
}

void chip8_system::_skip_if_vx_up(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const c8key key = static_cast<c8key>(_V[x]);
    if(_input->is_key_up(key))
        _PC += _instruction_size_in_bytes;
}

void chip8_system::_set_vx_to_dt(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _V[x] = _DT;
}

void chip8_system::_wait_for_vx_down(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const c8key key = _input->get_next_key_press();
    if(key == C8K_NONE)
        _PC = _PC - _instruction_size_in_bytes;
    else
        _V[x] = key;
}

void chip8_system::_set_dt_to_vx(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _DT = _V[x];
}

void chip8_system::_set_st_to_vx(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _ST = _V[x];
}

void chip8_system::_set_i_inc_vx(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _I += _V[x];
}

void chip8_system::_set_i_numeric_sprite_address(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _I = _numeric_addresses[_V[x]];
}

void chip8_system::_store_bcd_of_vx_at_i(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    uint8_t orig = _V[x];
    const uint8_t ones = orig % 10;
    orig /= 10;
    const uint8_t tens = orig % 10;
    orig /= 10;
    const uint8_t hundreds = orig % 10;
    _ram->store(&hundreds, sizeof(uint8_t), _I);
    _ram->store(&tens, sizeof(uint8_t), _I+1);
    _ram->store(&ones, sizeof(uint8_t), _I+2);
}

void chip8_system::_store_registers_at_i(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    _ram->store(_V.data(), x+1, _I);
}

void chip8_system::_load_registers_from_i(const uint16_t instruction) {
    const uint8_t x = (instruction & 0x0F00) >> 8;
    const auto register_data = _ram->load(_I, x+1);
    memcpy(_V.data(), register_data.get(), x+1);
}

chip8_system::chip8_system(std::shared_ptr<direct3d> d3d, std::shared_ptr<input> input)
    : _display(new display(d3d))
    , _ram(new ram())
    , _input(input)
    , _DT(0)
    , _ST(0)
    , _SPt(0x0000)
    , _PC(0x0200)
    , _I(0x0000)
    , _display_is_dirty(true)
{
    _create_numeric_sprite_data();
    srand(static_cast<unsigned>(time(0)));
}

void chip8_system::tick() {
    const uint16_t instruction = _ram->fetch_instruction(_PC);
    _PC += 2;
    _interpret_instruction(instruction);
}

void chip8_system::decrement_timers(const double dt) {
    static double time_elapsed = 0.0;
    time_elapsed += dt;
    const uint8_t ticks = static_cast<uint8_t>(time_elapsed * 60.0);
    if(_DT) {
        _DT = _DT < ticks ? 0 : _DT - ticks;
    }
    if(_ST) {
        _ST = _ST < ticks ? 0 : _ST - ticks;
    }
    time_elapsed -= ticks / 60.0;
}

void chip8_system::load_program_into_ram(const std::vector<uint8_t>& program_data) {
    _ram->load_program_data(program_data);
}

bool chip8_system::display_is_dirty() const {
    return _display_is_dirty;
}

void chip8_system::display_rendered() {
    _display_is_dirty = false;
}

void chip8_system::reset() {
    _ram.reset(new ram());
    _create_numeric_sprite_data();
    memset(_V.data(), 0, _V.size());
    _DT = 0;
    _ST = 0;
    _SPt = 0;
    _PC = 0x200;
    _I = 0;
    _display->clear();
    _display_is_dirty = true;
}

std::shared_ptr<display> chip8_system::get_display() {
    return _display;
}

uint8_t chip8_system::get_st() const {
    return _ST;
}
