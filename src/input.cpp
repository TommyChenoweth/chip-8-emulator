#include "input.hpp"

void input::_init_keymap() {
    _vk_to_c8k = {
        {'0', C8K_0},
        {'1', C8K_1},
        {'2', C8K_2},
        {'3', C8K_3},
        {'4', C8K_4},
        {'5', C8K_5},
        {'6', C8K_6},
        {'7', C8K_7},
        {'8', C8K_8},
        {'9', C8K_9},
        {'A', C8K_A},
        {'B', C8K_B},
        {'C', C8K_C},
        {'D', C8K_D},
        {'E', C8K_E},
        {'F', C8K_F},
    };
}

input::input() 
    : _capture_next_key_press(false)
    , _next_key_press(C8K_NONE)
{
    _init_keymap();
}

void input::set_key_pressed(const uint32_t virtual_key) {
    if(_capture_next_key_press)
        _next_key_press = _vk_to_c8k[virtual_key];
    set_key_down(virtual_key);
}

void input::set_key_down(const uint32_t virtual_key) {
    auto iter = _vk_to_c8k.find(virtual_key);
    if(iter != _vk_to_c8k.end()) {
        const c8key c8_key = std::get<1>(*iter);
        _key_down[c8_key] = true;
    }
}

void input::set_key_up(const uint32_t virtual_key) {
    auto iter = _vk_to_c8k.find(virtual_key);
    if(iter != _vk_to_c8k.end()) {
        const c8key c8_key = std::get<1>(*iter);
        if(c8_key == _next_key_press)
            _next_key_press = C8K_NONE;
        _key_down[c8_key] = false;
    }
}

bool input::is_key_down(const c8key key) const {
    return _key_down[key];
}

bool input::is_key_up(const c8key key) const {
    return !_key_down[key];
}

c8key input::get_next_key_press() {
    if(_next_key_press == C8K_NONE) {
        _capture_next_key_press = true;
        return C8K_NONE;
    }
    else {
        _capture_next_key_press = false;
        auto key = _next_key_press;
        _next_key_press = C8K_NONE;
        return key;
    }
}
