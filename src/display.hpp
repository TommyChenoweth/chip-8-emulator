#pragma once

#include <d3d11.h>
#include "direct3d.hpp"
#include "display_vertex.hpp"
#include <vector>

class display {
private:
    unsigned                                  _width;
    unsigned                                  _height;
    std::shared_ptr<ID3D11Texture2D>          _texture;
    std::vector<UINT>                         _buffer;
    std::shared_ptr<ID3D11SamplerState>       _sampler;
    std::shared_ptr<ID3D11ShaderResourceView> _texture_srv;

    std::wstring                              _shader_filename;
    std::string                               _vertex_shader_entry_point;
    std::string                               _pixel_shader_entry_point;
    std::shared_ptr<ID3DBlob>                 _shader_blob;
    std::shared_ptr<ID3D11VertexShader>       _vertex_shader;
    std::shared_ptr<ID3D11PixelShader>        _pixel_shader;
    std::vector<D3D11_INPUT_ELEMENT_DESC>     _input_desc;
    std::shared_ptr<ID3D11InputLayout>        _input_layout;
    std::shared_ptr<ID3D11Buffer>             _vertex_buffer;

    void _create_texture(std::shared_ptr<direct3d> d3d);
    void _create_vertex_buffer(std::shared_ptr<direct3d> d3d);

public:
    display(std::shared_ptr<direct3d> d3d);

    void clear();
    bool render_sprite(const std::shared_ptr<void> sprite_data, const size_t size, const int x, const int y);

    void update_texture(ID3D11DeviceContext* device_context);

    std::shared_ptr<ID3D11Texture2D> get_texture();
    std::shared_ptr<ID3D11ShaderResourceView> get_texture_srv();
    std::shared_ptr<ID3D11SamplerState> get_sampler();
    std::shared_ptr<ID3D11VertexShader> get_vertex_shader();
    std::shared_ptr<ID3D11PixelShader> get_pixel_shader();
    std::shared_ptr<ID3D11InputLayout> get_input_layout();
    std::shared_ptr<ID3D11Buffer> get_vertex_buffer();
};
