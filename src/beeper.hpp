#pragma once

#include <vector>
#include "wasapi.hpp"

class beeper {
private:
    struct frame_data {
        float left;
        float right;
    };

    static const uint32_t FADE_NONE = 0x0;
    static const uint32_t FADE_IN   = 0x1;
    static const uint32_t FADE_OUT  = 0x2;

    std::shared_ptr<IAudioClient>        _dd_audio_client;
    std::shared_ptr<IAudioRenderClient>  _dd_render_client;

    uint64_t                             _samples_per_second;
    uint32_t                             _num_channels;
    uint32_t                             _max_num_frames;

    float                                _amplitude;
    float                                _hertz;
    std::vector<frame_data>              _beep_audio_data;
    const frame_data*                    _start;
    const frame_data*                    _end;
    const frame_data*                    _next;
    double                               _buffer_refill_interval_s;
    double                               _tick_duration_s;
    double                               _frame_duration_s;
    uint32_t                             _frames_per_tick;
    double                               _latency_s;
    uint32_t                             _frames_within_latency;
    uint32_t                             _far_frames;
    uint32_t                             _preferred_num_fade_frames;

    bool                                 _beeping;

    void _build_beep_audio_data();
    void _copy_next_n_frames_to_buffer(const uint32_t n, void* const buffer);
    void _copy_audio_data_to_client(const uint32_t beep_frames, const uint32_t silence_frames = 0, const uint32_t fade = FADE_NONE);

public:
    beeper(std::shared_ptr<wasapi> wasapi);

    void beep(const double dt, const uint32_t st);
};
