#include "emulator_exception.hpp"

emulator_exception::emulator_exception(const std::string & what_arg)
    : runtime_error(what_arg)
{
}

emulator_exception::emulator_exception(const char * what_arg)
    : runtime_error(what_arg)
{
}
