#include "timer.hpp"

#include "com_pointer.hpp"
#include <spdlog/spdlog.h>
#include <Windows.h>

uint64_t timer::_get_counts() {
    /* This function is used in the class initialization list. DO NOT USE MEMBER
       VARIABLES HERE! */

    uint64_t counts = 0;
    QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&counts));
    return counts;
}

timer::timer()
    : _fps(0.0)
    , _mspf(0.0)
    , _paused(false)
    , _resumed(false)
    , _counts_this_frame(_get_counts())
    , _counts_last_frame(_counts_this_frame)
    , _seconds_per_count(0.0)
    , _dt(0.0)
{
    uint64_t counts_per_second = 0;
    QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&counts_per_second));
    _seconds_per_count = 1.0/static_cast<double>(counts_per_second);
}


void timer::pause() {
    _paused = true;
    spdlog::get(L"logger")->debug(L"Pausing...");
}

void timer::resume() {
    _paused = false;
    _resumed = true;
    spdlog::get(L"logger")->debug(L"Resuming...");
}

bool timer::is_paused() const {
    return _paused;
}

void timer::update() {
    static double time_elapsed = 0.0;
    static uint32_t number_of_frames = 0;

    if(_resumed) {
        _counts_this_frame = _get_counts();
        _counts_last_frame = _counts_this_frame;
        _resumed = false;
    }
    else {
        _counts_last_frame = _counts_this_frame;
        _counts_this_frame = _get_counts();
    }

    _dt = (_counts_this_frame - _counts_last_frame) * _seconds_per_count;
    time_elapsed += _dt;

    ++number_of_frames;

    const bool at_least_one_second_elapsed(1.0 <= time_elapsed);
    if(at_least_one_second_elapsed) {
        time_elapsed = 0;

        _fps = static_cast<double>(number_of_frames);
        number_of_frames = 0;

        _mspf = 1000.0 / _fps;
    }
}

double timer::get_dt() const {
    return _dt;
}

double timer::get_fps() const {
    return _fps;
}

double timer::get_mspf() const {
    return _mspf;
}