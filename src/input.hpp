#pragma once

#include <array>
#include <cstdint>
#include <unordered_map>

enum c8key {
    C8K_0,
    C8K_1,
    C8K_2,
    C8K_3,
    C8K_4,
    C8K_5,
    C8K_6,
    C8K_7,
    C8K_8,
    C8K_9,
    C8K_A,
    C8K_B,
    C8K_C,
    C8K_D,
    C8K_E,
    C8K_F,
    C8K_NONE,
};

class input {
private:
    std::array<bool, C8K_NONE>          _key_down;
    std::unordered_map<uint32_t, c8key> _vk_to_c8k;
    bool                                _capture_next_key_press;
    c8key                               _next_key_press;

    void _init_keymap();

public:
    input();

    void set_key_pressed(const uint32_t virtual_key);
    void set_key_down(const uint32_t virtual_key);
    void set_key_up(const uint32_t virtual_key);
    bool is_key_down(const c8key key) const;
    bool is_key_up(const c8key key) const;
    c8key get_next_key_press();
};