#include "d3d_exception.hpp"
#include "emulator.hpp"
#include <iostream>
#include <memory>
#include <spdlog/spdlog.h>
#include <Windows.h>
#include "windows_exception.hpp"

void create_and_attach_console();

int WINAPI wWinMain(HINSTANCE instance_handle, HINSTANCE instance_handle_prev, LPWSTR command_line_args, int show) {
    // Enable run-time memory check and console for debug builds.
#if defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    create_and_attach_console();
#endif
    try {
        const size_t q_size = 1048576;
        spdlog::set_async_mode(q_size);
        std::vector<spdlog::sink_ptr> sinks;
#if defined(_DEBUG)
        sinks.push_back(std::make_shared<spdlog::sinks::stdout_sink_mt>());
#endif
        sinks.push_back(std::make_shared<spdlog::sinks::rotating_file_sink_mt>(L"logs/combo_log", L"txt", 1024 * 500, 3));
        auto combined_logger = std::make_shared<spdlog::logger>(L"logger", begin(sinks), end(sinks));
        combined_logger->set_pattern(L"[%H:%M:%S.%e] [%l] %v");
        spdlog::register_logger(combined_logger);
    }
    catch(const spdlog::spdlog_ex& e) {
        std::cerr << e.what() << std::endl;
    }

    try {
        std::unique_ptr<emulator> emu(new emulator(instance_handle));
        emu->run();
    }
    catch(const d3d_exception& e) {
        auto logger = spdlog::get(L"logger");
        logger->error(L"{}\n{}\n{}\n{}", e.what(), e.windows_message(), e.location(), e.debug_messages());
    }
    catch(const windows_exception& e) {
        auto logger = spdlog::get(L"logger");
        logger->error(L"{}\n{}\n{}", e.what(), e.windows_message(), e.location());
    }
    spdlog::drop_all();

    return 0;
}

void create_and_attach_console() {
    AllocConsole();
    AttachConsole(GetCurrentProcessId());
    SetConsoleTitle(L"Debug Console");

    // Redirect output to the attached console.
    FILE* stdout_stream(nullptr);
    freopen_s(&stdout_stream, "CON", "w", stdout);
    FILE* stderr_stream(nullptr);
    freopen_s(&stderr_stream, "CON", "w", stderr);
    std::ios::sync_with_stdio();

    // Disable the console's close button as this seems to cause the application
    // to close ungracefully.
    RemoveMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
    DrawMenuBar(GetConsoleWindow());
}