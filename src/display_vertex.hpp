#pragma once

#include <directxmath.h>

struct display_vertex {
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT2 uv;

    display_vertex(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT2& uv)
        : pos(pos)
        , uv(uv)
    {}
};
