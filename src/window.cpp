#include "window.hpp"

LRESULT CALLBACK main_wnd_proc(HWND window_handle, UINT message, WPARAM w_param, LPARAM l_param);

HRESULT window::_register_class() {
    _class.cbSize = sizeof(WNDCLASSEX);
    _class.style = CS_HREDRAW | CS_VREDRAW;
    _class.lpfnWndProc = main_wnd_proc;
    _class.cbClsExtra = 0;
    _class.cbWndExtra = 0;
    _class.hInstance = _instance_handle;
    _class.hIcon = NULL;
    _class.hCursor = LoadCursor(NULL, IDC_ARROW);
    _class.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    _class.lpszMenuName = NULL;
    _class.lpszClassName = _class_name.c_str();
    _class.hIconSm = NULL;

    if(_icon_path != L"") {
        _class.hIcon = LoadIcon(_instance_handle, _icon_path.c_str());
        _class.hIconSm = LoadIcon(_class.hInstance, _icon_path.c_str());
    }

    if(!RegisterClassEx(&_class))
        return E_FAIL;
    return S_OK;
}

HRESULT window::_create_window() {
    RECT window_dimensions = {0, 0, _client_width, _client_height};
    AdjustWindowRect(&window_dimensions, WS_OVERLAPPEDWINDOW, FALSE);
    _window_handle = CreateWindow(_class_name.c_str(), 
                                  _window_title.c_str(), 
                                  WS_OVERLAPPEDWINDOW,
                                  CW_USEDEFAULT, 
                                  CW_USEDEFAULT, 
                                  window_dimensions.right - window_dimensions.left, 
                                  window_dimensions.bottom - window_dimensions.top, 
                                  NULL, 
                                  NULL, 
                                  _instance_handle,
                                  this);

    SetWindowLong(_window_handle, GWL_USERDATA, reinterpret_cast<LONG>(this));

    if(!_window_handle)
        return E_FAIL;

    return S_OK;
}

window::window(HINSTANCE instance_handle, const std::wstring& title, int client_width, int client_height)
    : _instance_handle(instance_handle)
    , _window_handle(0)
    , _menu()
    , _class_name(L"IGudClas")
    , _icon_path(L"")
    , _window_title(title)
    , _client_width(client_width)
    , _client_height(client_height)
{
    ZeroMemory(&_class, sizeof(WNDCLASSEX));

    // The order of these function calls is important. The class must be registered before
    // the window can be created.
    _register_class();
    _create_window();
}

void window::show() {
    ShowWindow(_window_handle, SW_SHOWNORMAL);
}

void window::attach_menu(menu& menu) {
    _menu = menu;
    SetMenu(_window_handle, _menu.get_menu_handle());
}

LRESULT window::wnd_proc(UINT message, WPARAM w_param, LPARAM l_param) {
    PAINTSTRUCT ps;
    HDC hdc;

    if(message == WM_SIZE && w_param == SIZE_RESTORED) {
        _client_width = LOWORD(l_param);
        _client_height = HIWORD(l_param);
    }

    if(_user_wnd_proc)
        return _user_wnd_proc(_window_handle, message, w_param, l_param);

    switch(message) {
        case WM_PAINT:
            hdc = BeginPaint(_window_handle, &ps);
            EndPaint(_window_handle, &ps);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        default:
            return DefWindowProc(_window_handle, message, w_param, l_param);
    }

    return 0;
}

HWND window::get_hwnd() {
    return _window_handle;
}

unsigned window::get_client_width() const {
    return _client_width;
}

unsigned window::get_client_height() const {
    return _client_height;
}

void window::set_wnd_proc(std::function<LRESULT(HWND, UINT, WPARAM, LPARAM)> wnd_proc) {
    _user_wnd_proc = wnd_proc;
}

LRESULT CALLBACK main_wnd_proc(HWND window_handle, UINT message, WPARAM w_param, LPARAM l_param) {
    auto window_obj = reinterpret_cast<window*>(GetWindowLong(window_handle, GWL_USERDATA));

    if(window_obj)
        return window_obj->wnd_proc(message, w_param, l_param);
    else
        return DefWindowProc(window_handle, message, w_param, l_param);
}