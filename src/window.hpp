#pragma once

#include <functional>
#include <memory>
#include "menu.hpp"
#include <string>
#include <Windows.h>

class window
{
private:
    HINSTANCE    _instance_handle;
    HWND         _window_handle;

    menu         _menu;

    WNDCLASSEX   _class;
    std::wstring _class_name;
    std::wstring _icon_path;
    std::wstring _window_title;

    int          _client_width;
    int          _client_height;

    HRESULT _register_class();
    HRESULT _create_window();

    std::function<LRESULT(HWND, UINT, WPARAM, LPARAM)> _user_wnd_proc;

public:
    window(HINSTANCE instance_handle, const std::wstring& title, int client_width, int client_height);

    LRESULT wnd_proc(UINT message, WPARAM w_param, LPARAM l_param);

    void show();
    void attach_menu(menu& menu);

    void set_wnd_proc(std::function<LRESULT(HWND, UINT, WPARAM, LPARAM)> wnd_proc);
    HWND get_hwnd();
    unsigned get_client_width() const;
    unsigned get_client_height() const;
};