#include "emulator.hpp"

#include "com_pointer.hpp"
#include <ShObjIdl.h>
#include <spdlog/spdlog.h>

void emulator::_set_custom_wnd_proc() {
    auto custom_wnd_proc = [this](HWND window_handle, UINT message, WPARAM w_param, LPARAM l_param) -> LRESULT {
        switch(message)
        {
            case WM_DESTROY:
                PostQuitMessage(0);
                break;

            case WM_MOVING:
                _frame_timer->pause();
                _instruction_timer->pause();
                break;

            case WM_EXITSIZEMOVE:
                _frame_timer->resume();
                _instruction_timer->resume();
                break;

            case WM_KEYDOWN: {
                bool pressed = !(l_param & (1 << 30));
                if(pressed)
                    _input->set_key_pressed(w_param);
                break;
            }

            case WM_KEYUP:
                _input->set_key_up(w_param);
                break;

            case WM_COMMAND: {
                auto type = HIWORD(w_param);
                auto index = LOWORD(w_param);
                if(type == 0)
                    menu::invoke_handler_for_menu_id(index);
                break;
            }

            case WM_ENTERMENULOOP:
                _frame_timer->pause();
                _instruction_timer->pause();
                break;

            case WM_EXITMENULOOP:
                _frame_timer->resume();
                _instruction_timer->resume();
                break;

            default:
                return DefWindowProc(window_handle, message, w_param, l_param);
        }

        return 0;
    };

    _window->set_wnd_proc(custom_wnd_proc);
}

void emulator::_build_and_attach_menu() {
    menu main_menu;

    menu file_dropdown;
    file_dropdown.append_item(L"Load", [&]() {
        IFileDialog* file_dialog = nullptr;
        HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&file_dialog));
        if(FAILED(hr)) return;
        auto open_file_dialog = make_com_pointer(file_dialog);

        std::vector<wchar_t> working_directory(128, 0);
        GetCurrentDirectory(working_directory.size(), &working_directory[0]);

        IShellItem* default_folder_raw = nullptr;
        hr = SHCreateItemFromParsingName(&working_directory[0], nullptr, IID_IShellItem, reinterpret_cast<void**>(&default_folder_raw));
        if(FAILED(hr)) return;
        auto default_folder = make_com_pointer(default_folder_raw);

        hr = open_file_dialog->SetFolder(default_folder.get());
        if(FAILED(hr)) return;

        hr = open_file_dialog->Show(_window->get_hwnd());
        if(FAILED(hr)) return;

        IShellItem* result_raw = nullptr;
        hr = open_file_dialog->GetResult(&result_raw);
        if(FAILED(hr)) return;
        auto result = make_com_pointer(result_raw);

        PWSTR file_path_raw = nullptr;
        hr = result->GetDisplayName(SIGDN_FILESYSPATH, &file_path_raw);
        if(FAILED(hr)) return;
        auto file_path = std::shared_ptr<WCHAR>(file_path_raw, [](PWSTR p) { CoTaskMemFree(p); });

        size_t chars_converted = 0;
        std::vector<char> char_string(256, 0);
        wcstombs_s(&chars_converted, &char_string[0], char_string.size(), file_path.get(), lstrlenW(file_path.get()));
        _path_to_program = std::string(&char_string[0]);
        _load_program_from_file(_path_to_program);
    });
    file_dropdown.append_item(L"Reload", [&]() {
        if(_path_to_program.empty())
            _load_placeholder_program();
        else
            _load_program_from_file(_path_to_program);
    });
    file_dropdown.append_item(L"Close", [&]() {
        _path_to_program.clear();
        _load_placeholder_program();
    });
    main_menu.append_dropdown(L"File", file_dropdown);

    menu debug_dropdown;
    static menu logging_level;
    logging_level.append_item(L"Info", [&]() {
        spdlog::get(L"logger")->set_level(spdlog::level::info);
        logging_level.check_menu_item(L"Info");
        logging_level.uncheck_menu_item(L"Debug");
    }, MF_STRING | MF_CHECKED);
    logging_level.append_item(L"Debug", [&]() {
        spdlog::get(L"logger")->set_level(spdlog::level::debug);
        logging_level.check_menu_item(L"Debug");
        logging_level.uncheck_menu_item(L"Info");
    });
    debug_dropdown.append_dropdown(L"Logging Level", logging_level);
    main_menu.append_dropdown(L"Debug", debug_dropdown);

    _window->attach_menu(main_menu);
}

void emulator::_tweak_seconds_per_instruction(const double dt) {
    if(_instructions_last_frame) {
        const double last_average_spi = dt / _instructions_last_frame;
        _seconds_per_instruction = _min_seconds_per_instruction <= last_average_spi ? last_average_spi : _seconds_per_instruction;
    }
}

void emulator::_load_program_from_file(const std::string & path) {
    _chip8->reset();

    FILE* program = nullptr;
    fopen_s(&program, path.c_str(), "rb");
    if(!program) throw std::runtime_error("Failed to open program file.");
    fseek(program, 0, SEEK_END);
    long file_size = ftell(program);
    fseek(program, 0, SEEK_SET);
    std::vector<uint8_t> program_data(file_size, 0);
    fread(&program_data[0], sizeof(uint8_t), program_data.size(), program);
    fclose(program);

    spdlog::get(L"logger")->debug(L"Loading file: {}", path);

    _chip8->load_program_into_ram(program_data);
}

void emulator::_load_placeholder_program() {
    _chip8->reset();

    static std::vector<uint8_t> placeholder_program;
    if(placeholder_program.empty()) {
        std::vector<uint16_t> data = {
            0x6010, 0x610C, 0x6305, 0x6400, 0x6502, 0x6706, 0xA22E, 0xD015,
            0x7006, 0x7401, 0xF31E, 0x8640, 0x8655, 0x3F00, 0x1228, 0x8640,
            0x8675, 0x3F00, 0x1224, 0x120E, 0x65FF, 0x7003, 0x120E, 0x88C8,
            0xA898, 0x88F8, 0x8888, 0x88F8, 0xF888, 0xF890, 0x88F8, 0x8888,
            0x88F8, 0x88D8, 0xA888, 0x8800,
        };
        for(uint16_t& word : data) {
            word = _byteswap_ushort(word);
        }
        placeholder_program.resize(data.size()*2, 0);
        memcpy(&placeholder_program[0], &data[0], placeholder_program.size());
    }

    _chip8->load_program_into_ram(placeholder_program);
}

void emulator::_update_window_title(const double dt) {
    static double time_elapsed = 0.0;
    time_elapsed += dt;
    if(_seconds_per_title_update <= time_elapsed) {
        static std::vector<wchar_t> title(64, 0);
        const double speed = static_cast<double>(_instructions_since_last_title_update) / static_cast<double>(_instructions_per_second) * 100.0;
        swprintf_s(&title[0],title.size(), L"Chip 8 Emulator - Speed: %.0f%% - Instructions: %d", speed, _instructions_since_last_title_update);
        SetWindowText(_window->get_hwnd(), &title[0]);

        _instructions_since_last_title_update = 0;
        time_elapsed -= _seconds_per_title_update;
    }
}

emulator::emulator(HINSTANCE instance_handle)
    : _window(new window(instance_handle, L"Chip 8 Emulator", 640, 320))
    , _d3d(new direct3d(_window->get_hwnd()))
    , _input(new input())
    , _chip8(new chip8_system(_d3d, _input))
    , _display(_chip8->get_display())
    , _frame_timer(new timer())
    , _instruction_timer(new timer())
    , _wasapi(new wasapi())
    , _beeper(new beeper(_wasapi))
    , _instructions_per_second(600)
    , _min_seconds_per_instruction(1.0 / _instructions_per_second)
    , _seconds_per_instruction(_min_seconds_per_instruction)
    , _seconds_per_title_update(1.0)
    , _instructions_last_frame(0)
    , _instructions_since_last_title_update(0)
    , _path_to_program("")
{
    _set_custom_wnd_proc();
    _build_and_attach_menu();
    _window->show();
    _load_placeholder_program();
}

void emulator::run() {
    ID3D11Device* device = _d3d->get_device();
    ID3D11DeviceContext* device_context = _d3d->get_device_context();
    ID3D11RenderTargetView* render_target_view = _d3d->get_render_target_view();
    IDXGISwapChain* swap_chain = _d3d->get_swap_chain();
    const float clear_color[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; //red,green,blue,alpha

    ID3D11InputLayout* input_layout = _display->get_input_layout().get();
    device_context->IASetInputLayout(input_layout);
    const UINT stride = sizeof(display_vertex);
    const UINT offset = 0;
    ID3D11Buffer* vertex_buffer = _display->get_vertex_buffer().get();
    device_context->IASetVertexBuffers(0, 1, &vertex_buffer, &stride, &offset);
    device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

    ID3D11ShaderResourceView* texture_srv = _display->get_texture_srv().get();
    ID3D11SamplerState* sampler = _display->get_sampler().get();

    MSG msg = {0};
    while(WM_QUIT != msg.message) {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else {
            _frame_timer->update();
            const double dt = _frame_timer->get_dt();

            _chip8->decrement_timers(dt);

            _instruction_timer->update();

            static double accumulated_time = 0.0;
            accumulated_time += dt;
            const uint32_t instructions_to_execute = static_cast<uint32_t>(accumulated_time / _seconds_per_instruction);
            for(uint32_t i = 0; i < instructions_to_execute; ++i) {
                _chip8->tick();
            }
            _instructions_since_last_title_update += instructions_to_execute;
            _instructions_last_frame = instructions_to_execute;
            accumulated_time -= instructions_to_execute * _seconds_per_instruction;

            _instruction_timer->update();
            _tweak_seconds_per_instruction(_instruction_timer->get_dt());

            _beeper->beep(dt, _chip8->get_st());

            if(_chip8->display_is_dirty()) {
                _display->update_texture(device_context);

                device_context->ClearRenderTargetView(render_target_view, clear_color);

                device_context->PSSetShader(_display->get_pixel_shader().get(), nullptr, 0);
                device_context->VSSetShader(_display->get_vertex_shader().get(), nullptr, 0);
                device_context->PSSetShaderResources(0, 1, &texture_srv);
                device_context->PSSetSamplers(0, 1, &sampler);
                device_context->Draw(4, 0);

                swap_chain->Present(0, 0);

                _chip8->display_rendered();
            }

            _update_window_title(dt);
        }
    }
}
