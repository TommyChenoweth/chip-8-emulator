#include "display.hpp"

#include "com_pointer.hpp"
#include "d3d_exception.hpp"

void display::_create_texture(std::shared_ptr<direct3d> d3d) {
    const DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;

    D3D11_TEXTURE2D_DESC texture_desc;
    memset(&texture_desc, 0, sizeof(texture_desc));
    texture_desc.Width = _width;
    texture_desc.Height = _height;
    texture_desc.MipLevels = 1;
    texture_desc.ArraySize = 1;
    texture_desc.Format = format;
    texture_desc.Usage = D3D11_USAGE_DEFAULT;
    texture_desc.CPUAccessFlags = 0;
    texture_desc.SampleDesc.Count = 1;
    texture_desc.SampleDesc.Quality = 0;
    texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    ID3D11Device* device = d3d->get_device();
    ID3D11Texture2D* texture = nullptr;
    HRESULT hr = device->CreateTexture2D(&texture_desc, nullptr, &texture);
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to create texture for display.", hr, device);
    _texture = make_com_pointer(texture);

    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
    memset(&srv_desc, 0, sizeof(srv_desc));
    srv_desc.Format = format;
    srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srv_desc.Texture2D.MipLevels = 1;
    ID3D11ShaderResourceView* texture_srv = nullptr;
    hr = device->CreateShaderResourceView(texture, &srv_desc, &texture_srv);
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to create texture srv for display.", hr, device);
    _texture_srv = make_com_pointer(texture_srv);
}

void display::_create_vertex_buffer(std::shared_ptr<direct3d> d3d) {
    std::vector<display_vertex> initial_data;
    initial_data.emplace_back(DirectX::XMFLOAT3(-1.0f, -1.0f, 0.5f), DirectX::XMFLOAT2(0.0f, 1.0f));
    initial_data.emplace_back(DirectX::XMFLOAT3(-1.0f, 1.0f, 0.5f), DirectX::XMFLOAT2(0.0f, 0.0f));
    initial_data.emplace_back(DirectX::XMFLOAT3(1.0f, -1.0f, 0.5f), DirectX::XMFLOAT2(1.0f, 1.0f));
    initial_data.emplace_back(DirectX::XMFLOAT3(1.0f, 1.0f, 0.5f), DirectX::XMFLOAT2(1.0f, 0.0f));

    D3D11_BUFFER_DESC bd;
    memset(&bd, 0, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(display_vertex) * initial_data.size();
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

    D3D11_SUBRESOURCE_DATA init_data;
	memset(&init_data, 0, sizeof(init_data));
    init_data.pSysMem = &initial_data[0];

    ID3D11Device* device = d3d->get_device();
    ID3D11Buffer* vertex_buffer = nullptr;
    HRESULT hr = device->CreateBuffer(&bd, &init_data, &vertex_buffer);
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to create vertex buffer for display.", hr, device);
    _vertex_buffer = make_com_pointer(vertex_buffer);
}

display::display(std::shared_ptr<direct3d> d3d)
    : _width(64)
    , _height(32)
    , _texture(nullptr)
    , _buffer(_width * _height, 0xFFFFFFFF)
    , _shader_filename(L"../shaders/display.fx")
    , _vertex_shader_entry_point("VS")
    , _pixel_shader_entry_point("PS")
    , _shader_blob(nullptr)
    , _vertex_shader(nullptr)
    , _pixel_shader(nullptr)
    , _input_layout(nullptr)
{
    _create_texture(d3d);

    D3D11_SAMPLER_DESC sample_desc;
    memset(&sample_desc, 0, sizeof(sample_desc));
    sample_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
    sample_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sample_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sample_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sample_desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sample_desc.MinLOD = 0;
    sample_desc.MaxLOD = D3D11_FLOAT32_MAX;
    ID3D11Device* device = d3d->get_device();
    ID3D11SamplerState* sampler_state = nullptr;
    HRESULT hr = device->CreateSamplerState(&sample_desc, &sampler_state);
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to create sampler state for display.", hr, device);
    _sampler = make_com_pointer(sampler_state);

    _vertex_shader = d3d->compile_vertex_shader_from_file(_shader_filename, _vertex_shader_entry_point, "vs_4_0", _shader_blob);
    _pixel_shader = d3d->compile_pixel_shader_from_file(_shader_filename, _pixel_shader_entry_point, "ps_4_0");

    _input_desc = {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
    };

    ID3D11InputLayout* input_layout = nullptr;
    hr = device->CreateInputLayout(_input_desc.data(), _input_desc.size(), _shader_blob->GetBufferPointer(), _shader_blob->GetBufferSize(), &input_layout);
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to create input layout for display.", hr, device);
    _input_layout = make_com_pointer(input_layout);

    _create_vertex_buffer(d3d);
}

void display::clear() {
    memset(&_buffer[0], 0xFFFFFFFF, _buffer.size()*sizeof(UINT));
}

bool display::render_sprite(const std::shared_ptr<void> sprite_data, const size_t size, const int x, const int y) {
    const char* const raw_data = static_cast<char*>(sprite_data.get());
    const unsigned sprite_rows = size;
    const unsigned bits_in_byte = 8;
    bool collision = false;
    for(unsigned display_row = y, sprite_row = 0; sprite_row < sprite_rows; ++display_row, ++sprite_row) {
        for(unsigned col = x, bit = 0x80; 0 < bit; ++col, bit >>= 1) {
            const unsigned wrapped_row = display_row % _height;
            const unsigned wrapped_col = col % _width;
            const unsigned i = wrapped_row * _width + wrapped_col;
            const UINT curr_texel_color = _buffer[i] & 0x00FFFFFF;
            const UINT sprite_texel_color = (bit & raw_data[sprite_row]) ? 0x00000000 : 0xFFFFFFFF;
            const UINT new_texel_color = ~(~curr_texel_color ^ ~sprite_texel_color);
            _buffer[i] = new_texel_color | 0xFF000000;
            if(!collision && !curr_texel_color && new_texel_color)
                collision = true;
        }
    }
    return collision;
}

void display::update_texture(ID3D11DeviceContext* device_context) {
    device_context->UpdateSubresource(_texture.get(),
                                      0,
                                      nullptr,
                                      &_buffer[0],
                                      _width * sizeof(UINT),
                                      0);
}

std::shared_ptr<ID3D11Texture2D> display::get_texture() {
    return _texture;
}

std::shared_ptr<ID3D11ShaderResourceView> display::get_texture_srv() {
    return _texture_srv;
}

std::shared_ptr<ID3D11SamplerState> display::get_sampler() {
    return _sampler;
}

std::shared_ptr<ID3D11VertexShader> display::get_vertex_shader() {
    return _vertex_shader;
}

std::shared_ptr<ID3D11PixelShader> display::get_pixel_shader() {
    return _pixel_shader;
}

std::shared_ptr<ID3D11InputLayout> display::get_input_layout() {
    return _input_layout;
}

std::shared_ptr<ID3D11Buffer> display::get_vertex_buffer() {
    return _vertex_buffer;
}
