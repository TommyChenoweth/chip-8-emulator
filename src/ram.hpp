#pragma once

#include <memory>
#include <stdint.h>
#include <vector>

class ram {
private:
    std::vector<char> _data;

public:
    ram();

    void load_program_data(std::vector<uint8_t> program_data, int offset = 0x200);
    unsigned fetch_instruction(const uint16_t address);
    void store(const void* data, const size_t size_in_bytes, const uint16_t address);
    std::shared_ptr<void> load(const uint16_t address, const size_t size_in_bytes);
};