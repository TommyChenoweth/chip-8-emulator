#include "d3d_exception.hpp"

#include <functional>
#include <memory>
#include <Windows.h>

#ifdef DX_DEPRECATED_ERROR_MESSAGES
#include <DxErr.h>
#endif

std::wstring d3d_exception::_retrieve_dx_message() {
#ifdef DX_DEPRECATED_ERROR_MESSAGES
    std::wstring dx_message(512, 0);
    swprintf_s(&dx_message[0], dx_message.size(), L"DXError: %s\nDescription: %s", DXGetErrorString(hr), DXGetErrorDescription(hr));
    return dx_message;
#else
    return _retrieve_windows_message();
#endif
}

d3d_exception::d3d_exception(const std::string & what_arg, const HRESULT hr)
    : windows_exception(what_arg.c_str(), hr)
    , _debug_messages("No debug messages available.")
{
    _set_windows_message(_retrieve_dx_message());
}

d3d_exception::d3d_exception(const char * what_arg, const HRESULT hr)
    : windows_exception(what_arg, hr)
    , _debug_messages("No debug messages available.")
{
    _set_windows_message(_retrieve_dx_message());
}

d3d_exception::d3d_exception(const char* what_arg, const HRESULT hr, const wchar_t* function, const wchar_t* file, int line)
    : windows_exception(what_arg, hr)
    , _debug_messages("No debug messages available.")
{
    _set_windows_message(_retrieve_dx_message());
}

d3d_exception::d3d_exception(const char * what_arg, const HRESULT hr, const wchar_t * function, const wchar_t * file, int line, ID3D11Device* device)
    : d3d_exception(what_arg, hr, function, file, line)
{
#ifdef _DEBUG
    ID3D11InfoQueue* info_queue = nullptr;
    if(SUCCEEDED(device->QueryInterface(__uuidof(ID3D11InfoQueue), reinterpret_cast<void**>(&info_queue)))) {
        UINT64 num_stored_messages = info_queue->GetNumStoredMessages();
        if(0 < num_stored_messages)
            _debug_messages.clear();
        for(UINT64 i = 0; i < num_stored_messages; ++i) {
            SIZE_T message_length = 0;
            info_queue->GetMessage(i, nullptr, &message_length);
            std::unique_ptr<D3D11_MESSAGE, std::function<void(D3D11_MESSAGE*)>> message(static_cast<D3D11_MESSAGE*>(malloc(message_length)), [](D3D11_MESSAGE* p) {free(p); });
            info_queue->GetMessage(i, message.get(), &message_length);
            _debug_messages += message->pDescription;
            _debug_messages += "\n";
        }
        info_queue->Release();
    }
#endif
}

const char * d3d_exception::debug_messages() const {
    return _debug_messages.c_str();
}
