#pragma once

#include <Audioclient.h>
#include <memory>
#include <mmdeviceapi.h>

class wasapi {
private:
    std::shared_ptr<IMMDeviceEnumerator> _device_enumerator;
    std::shared_ptr<IMMDevice>           _default_device;
    std::shared_ptr<IAudioClient>        _dd_audio_client;
    std::shared_ptr<IAudioRenderClient>  _dd_render_client;

    REFERENCE_TIME                       _reftimes_per_s;
    uint64_t                             _samples_per_second;
    uint32_t                             _num_channels;

    void _initialize();

public:
    wasapi(const REFERENCE_TIME reftimes_per_s = 10'000'000);

    std::shared_ptr<IAudioClient>       get_dd_audio_client();
    std::shared_ptr<IAudioRenderClient> get_dd_render_client();
    uint64_t get_samples_per_second() const;
    uint32_t get_num_channels() const;
};