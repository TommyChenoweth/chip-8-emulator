#pragma once

#include <d3d11.h>
#include <dxgi.h>
#include <memory>
#include <string>
#include <Windows.h>

class direct3d {
private:
    HWND                    _window_handle;
    long                    _backbuffer_width;
    long                    _backbuffer_height;
    float                   _aspect_ratio;
    D3D_DRIVER_TYPE         _driver_type;
    D3D_FEATURE_LEVEL       _feature_level;

    ID3D11Device*           _device;
    ID3D11DeviceContext*    _device_context;
    IDXGISwapChain*         _swap_chain;
    ID3D11RenderTargetView* _render_target_view;
    ID3D11Debug*            _debug;
    ID3D11InfoQueue*        _info_queue;

    void _initialize_direct3d();
    void _create_device_and_swapchain();
    void _get_debug_interfaces();
    void _create_render_target_view();
    void _create_viewport();

public:
    direct3d(HWND window_handle);
    ~direct3d();

    std::shared_ptr<ID3DBlob> compile_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model);
    std::shared_ptr<ID3D11VertexShader> direct3d::compile_vertex_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model, std::shared_ptr<ID3DBlob>& shader_blob);
    std::shared_ptr<ID3D11VertexShader> compile_vertex_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model);
    std::shared_ptr<ID3D11PixelShader> direct3d::compile_pixel_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model, std::shared_ptr<ID3DBlob>& shader_blob);
    std::shared_ptr<ID3D11PixelShader> compile_pixel_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model);

    float get_aspect_ratio() const;
    ID3D11Device* get_device();
    ID3D11DeviceContext* get_device_context();
    IDXGISwapChain* get_swap_chain();
    ID3D11RenderTargetView* get_render_target_view();
};