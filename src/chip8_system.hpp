#pragma once

#include <array>
#include "direct3d.hpp"
#include "display.hpp"
#include "input.hpp"
#include <memory>
#include "ram.hpp"

class chip8_system {
private:
    std::shared_ptr<display> _display;
    std::unique_ptr<ram>     _ram;
    std::shared_ptr<input>   _input;
    std::array<uint8_t, 16>  _V;
    uint8_t                  _DT;
    uint8_t                  _ST;
    uint16_t                 _SPt;
    uint16_t                 _PC;
    uint16_t                 _I;

    const uint16_t           _instruction_size_in_bytes = 2;
    const uint16_t           _address_size_in_bytes = 2;
    const uint16_t           _numeric_sprite_size_in_bytes = 5;

    const uint16_t           _numeric_addresses_start = 0x100;
    std::array<uint16_t, 16> _numeric_addresses;

    bool                     _display_is_dirty;

    void _create_numeric_sprite_data();

    void _interpret_instruction(const uint16_t instruction);
    void _push_address_to_stack(const uint16_t address);
    int _pop_address_from_stack();

    inline void _return_from_subroutine(const uint16_t instruction);
    inline void _jump(const uint16_t instruction);
    inline void _call_subroutine(const uint16_t instruction);
    inline void _skip_if_vx_eq_kk(const uint16_t instruction);
    inline void _skip_if_vx_ne_kk(const uint16_t instruction);
    inline void _skip_if_vx_eq_vy(const uint16_t instruction);
    inline void _set_vx_to_kk(const uint16_t instruction);
    inline void _set_vx_inc_by_kk(const uint16_t instruction);
    inline void _set_vx_to_vy(const uint16_t instruction);
    inline void _set_vx_to_vx_or_vy(const uint16_t instruction);
    inline void _set_vx_to_vx_and_vy(const uint16_t instruction);
    inline void _set_vx_to_vx_xor_vy(const uint16_t instruction);
    inline void _set_vx_to_vx_plus_vy(const uint16_t instruction);
    inline void _set_vx_to_vx_minus_vy(const uint16_t instruction);
    inline void _set_vx_to_rshift_vx(const uint16_t instruction);
    inline void _set_vx_to_vy_minus_vx(const uint16_t instruction);
    inline void _set_vx_to_lshift_vx(const uint16_t instruction);
    inline void _skip_if_vx_ne_vy(const uint16_t instruction);
    inline void _set_i_to_address(const uint16_t instruction);
    inline void _jump_offset_v0(const uint16_t instruction);
    inline void _set_vx_to_rand(const uint16_t instruction);
    inline void _draw_sprite_at_address(const uint16_t instruction);
    inline void _skip_if_vx_down(const uint16_t instruction);
    inline void _skip_if_vx_up(const uint16_t instruction);
    inline void _set_vx_to_dt(const uint16_t instruction);
    inline void _wait_for_vx_down(const uint16_t instruction);
    inline void _set_dt_to_vx(const uint16_t instruction);
    inline void _set_st_to_vx(const uint16_t instruction);
    inline void _set_i_inc_vx(const uint16_t instruction);
    inline void _set_i_numeric_sprite_address(const uint16_t instruction);
    inline void _store_bcd_of_vx_at_i(const uint16_t instruction);
    inline void _store_registers_at_i(const uint16_t instruction);
    inline void _load_registers_from_i(const uint16_t instruction);

public:
    chip8_system(std::shared_ptr<direct3d> d3d, std::shared_ptr<input> input);

    void tick();
    void decrement_timers(const double dt);
    void load_program_into_ram(const std::vector<uint8_t>& program_data);
    bool display_is_dirty() const;
    void display_rendered();
    void reset();

    std::shared_ptr<display> get_display();
    uint8_t get_st() const;
};