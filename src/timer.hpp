#pragma once

#include <stdint.h>

class timer
{
private:
    double   _fps;
    double   _mspf;

    bool     _paused;
    bool     _resumed;

    uint64_t _counts_this_frame;
    uint64_t _counts_last_frame;
    double   _seconds_per_count;

    double   _dt;

    uint64_t _get_counts();

public:
    timer();

    void pause();
    void resume();
    bool is_paused() const;

    void update();

    double get_dt() const;
    double get_fps() const;
    double get_mspf() const;
};