#pragma once

#include <stdexcept>

class emulator_exception : public std::runtime_error {
public:
    explicit emulator_exception(const std::string& what_arg);
    explicit emulator_exception(const char* what_arg);
};