#include "beeper.hpp"

#include <cmath>
#include <DirectXMath.h>
#include <iostream>
#include <spdlog/spdlog.h>
#include "windows_exception.hpp"

void beeper::_build_beep_audio_data() {
    for(size_t i = 0; i < _beep_audio_data.size(); ++i) {
        const float percent_of_second = static_cast<float>(i) / static_cast<float>(_samples_per_second);
        const float x = percent_of_second * DirectX::XM_2PI;
        auto& current = _beep_audio_data[i];
        current.left = _amplitude * sin(x * _hertz);
        current.right = current.left;
    }
}

void beeper::_copy_next_n_frames_to_buffer(const uint32_t n, void* const buffer) {
    uint32_t copy_size = 0;
    uint32_t total_copied_size = 0;
    while(total_copied_size < n) {
        copy_size = n - total_copied_size;
        const bool wrap = _end <= _next + copy_size;
        if(wrap)
            copy_size = _end - _next;
        memcpy(buffer, _next, copy_size * sizeof(frame_data));
        if(wrap)
            _next = _start;
        else
            _next = _next + copy_size;
        total_copied_size += copy_size;
    }
}

void beeper::_copy_audio_data_to_client(const uint32_t beep_frames, const uint32_t silence_frames, const uint32_t fade) {
    const uint32_t frames_to_copy = beep_frames + silence_frames;

    BYTE* buffer = nullptr;
    HRESULT hr = _dd_render_client->GetBuffer(frames_to_copy, &buffer);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to get audio client buffer.", hr);

    _copy_next_n_frames_to_buffer(beep_frames, buffer);

    frame_data* fd = reinterpret_cast<frame_data*>(buffer);

    for(uint32_t i = beep_frames; i < frames_to_copy; i++) {
        fd[i] = {};
    }

    if(fade & FADE_IN) {
        uint32_t fade_frames = _preferred_num_fade_frames < beep_frames ? _preferred_num_fade_frames : beep_frames;
        uint32_t fade_start = 0;
        for(uint32_t i = 0; i < fade_frames; ++i) {
            auto& current = fd[i + fade_start];
            float scale = static_cast<float>(i) / static_cast<float>(fade_frames);
            current.left *= scale;
            current.right = current.left;
        }
    }

    if(fade & FADE_OUT) {
        uint32_t fade_frames = _preferred_num_fade_frames < silence_frames ? _preferred_num_fade_frames : silence_frames;
        uint32_t fade_start = beep_frames;

        _copy_next_n_frames_to_buffer(fade_frames, fd + fade_start);

        for(uint32_t i = 0; i < fade_frames; ++i) {
            auto& current = fd[i + fade_start];
            float scale = (static_cast<float>(fade_frames) - static_cast<float>(i)) / static_cast<float>(fade_frames);
            current.left *= scale;
            current.right = current.left;
        }
    }

    if(0 < silence_frames)
        _next = _start;

    hr = _dd_render_client->ReleaseBuffer(frames_to_copy, 0);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to release audio client buffer.", hr);
}

beeper::beeper(std::shared_ptr<wasapi> wasapi)
    : _dd_audio_client(wasapi->get_dd_audio_client())
    , _dd_render_client(wasapi->get_dd_render_client())
    , _samples_per_second(wasapi->get_samples_per_second())
    , _num_channels(wasapi->get_num_channels())
    , _max_num_frames(0)
    , _amplitude(0.2f)
    , _hertz(500.0f)
    , _beep_audio_data(static_cast<uint32_t>(_samples_per_second))
    , _start(&_beep_audio_data[0])
    , _end(_start + _samples_per_second)
    , _next(_start)
    , _buffer_refill_interval_s(0.01)
    , _tick_duration_s(1.0 / 60.0)
    , _frame_duration_s(1.0 / _samples_per_second)
    , _frames_per_tick(static_cast<uint32_t>(_tick_duration_s / _frame_duration_s))
    , _latency_s(0.1)
    , _frames_within_latency(static_cast<uint32_t>(_latency_s / _frame_duration_s))
    , _far_frames(0)
    , _preferred_num_fade_frames(240)
{
    _build_beep_audio_data();

    HRESULT hr = _dd_audio_client->GetBufferSize(&_max_num_frames);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to get max buffer size of default device.", hr);

    _far_frames = _max_num_frames - _frames_within_latency;

    _copy_audio_data_to_client(0, 0, _frames_within_latency);

    hr = _dd_audio_client->Start();
    WINDOWS_EXCEPTION_IF_FAILED("Failed to start default device.", hr);
}

void beeper::beep(const double dt, const uint32_t st) {
    static bool last_was_beep = false;

    static double time_since_last_refill_s = 0.0;
    time_since_last_refill_s += dt;

    if(_buffer_refill_interval_s <= time_since_last_refill_s) {
        uint32_t num_padding_frames = 0;
        const HRESULT hr = _dd_audio_client->GetCurrentPadding(&num_padding_frames);
        WINDOWS_EXCEPTION_IF_FAILED("Failed to get padding frames.", hr);

        const uint32_t empty_frames = _max_num_frames - num_padding_frames;
        const int32_t near_frames = empty_frames - _far_frames;

        if(0 < near_frames) {
            const uint32_t beep_frames = st * _frames_per_tick;
            if(static_cast<int32_t>(beep_frames) < near_frames) {
                // The beep sound is either ending shortly or isn't playing at all.
                // Pad with silence.
                const uint32_t silence_frames = near_frames - beep_frames;
                uint32_t fade = last_was_beep || beep_frames && silence_frames ? FADE_OUT : FADE_NONE;
                fade |= !last_was_beep && beep_frames ? FADE_IN : FADE_NONE;
                _copy_audio_data_to_client(beep_frames, silence_frames, fade);
                last_was_beep = false;
            }
            else {
                // The beep sound will continue long enough to fill all near frames.
                const uint32_t fade = last_was_beep ? FADE_NONE : FADE_IN;
                _copy_audio_data_to_client(near_frames, 0, fade);
                last_was_beep = true;
            }
        }

        time_since_last_refill_s -= _buffer_refill_interval_s;
    }
}
