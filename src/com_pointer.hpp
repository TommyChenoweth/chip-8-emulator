#pragma once

#include <d3d9.h>
#include <memory>

template <typename T>
std::shared_ptr<T> make_com_pointer(T* com_object)
{
	return std::shared_ptr<T> (com_object, [](T* p){p->Release();});
}