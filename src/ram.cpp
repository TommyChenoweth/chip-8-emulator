#include "ram.hpp"

ram::ram()
    : _data(0xFFF, 0)
{
}

void ram::load_program_data(std::vector<uint8_t> program_data, int offset) {
    memcpy(&_data[offset], &program_data[0], program_data.size());
}

unsigned ram::fetch_instruction(const uint16_t address) {
    const uint16_t instruction = *reinterpret_cast<uint16_t*>(&_data[address]);
    return _byteswap_ushort(instruction);
}

void ram::store(const void* data, const size_t size_in_bytes, const uint16_t address) {
    memcpy(&_data[address], data, size_in_bytes);
}

std::shared_ptr<void> ram::load(const uint16_t address, const size_t size_in_bytes) {
    std::shared_ptr<void> requested_data(malloc(size_in_bytes), [](void* p){free(p);});
    memcpy(requested_data.get(), &_data[address], size_in_bytes);
    return requested_data;
}
