#include "direct3d.hpp"

#include <codecvt>
#include "com_pointer.hpp"
#include "d3d_exception.hpp"
#include <d3dcompiler.h>

void direct3d::_initialize_direct3d() {
    _create_device_and_swapchain();
    _get_debug_interfaces();
    _create_render_target_view();
    _create_viewport();
}

void direct3d::_create_device_and_swapchain() {
    RECT rc;
    GetClientRect(_window_handle, &rc);
    _backbuffer_width = rc.right - rc.left;
    _backbuffer_height = rc.bottom - rc.top;

    _aspect_ratio = static_cast<float>(_backbuffer_width) / static_cast<float>(_backbuffer_height);

    UINT create_device_flags = 0;
#ifdef _DEBUG
    create_device_flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_FEATURE_LEVEL feature_levels[] = {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
    UINT num_feature_levels = ARRAYSIZE(feature_levels);

    DXGI_SWAP_CHAIN_DESC sd;
    memset(&sd, 0, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = _backbuffer_width;
    sd.BufferDesc.Height = _backbuffer_height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = _window_handle;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    HRESULT hr = D3D11CreateDeviceAndSwapChain(nullptr, _driver_type, nullptr, create_device_flags, feature_levels, num_feature_levels,
                                               D3D11_SDK_VERSION, &sd, &_swap_chain, &_device, &_feature_level, &_device_context);

    if(FAILED(hr)) throw D3DEXCEPTION("Failed to create device + swap chain.", hr);
}

void direct3d::_get_debug_interfaces() {
    if(SUCCEEDED(_device->QueryInterface( __uuidof(ID3D11Debug), reinterpret_cast<void**>(&_debug)))) {
        if(SUCCEEDED(_debug->QueryInterface(__uuidof(ID3D11InfoQueue), reinterpret_cast<void**>(&_info_queue)))) {
            D3D11_MESSAGE_ID hide [] = {
                D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,
            };
 
            D3D11_INFO_QUEUE_FILTER filter;
            memset(&filter, 0, sizeof(filter));
            filter.DenyList.NumIDs = _countof(hide);
            filter.DenyList.pIDList = hide;
            _info_queue->AddStorageFilterEntries(&filter);
        }
    }
}

void direct3d::_create_render_target_view() {
    ID3D11Texture2D* back_buffer = nullptr;
    HRESULT hr = _swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<LPVOID*>(&back_buffer));
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to get swap chain buffer.", hr, _device);

    hr = _device->CreateRenderTargetView(back_buffer, nullptr, &_render_target_view);
    back_buffer->Release();
    if(FAILED(hr)) throw D3DEXCEPTION_D("Failed to create a render target view.", hr, _device);

    _device_context->OMSetRenderTargets(1, &_render_target_view, nullptr);
}

void direct3d::_create_viewport() {
    D3D11_VIEWPORT vp;
    vp.Width = static_cast<float>(_backbuffer_width);
    vp.Height = static_cast<float>(_backbuffer_height);
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    _device_context->RSSetViewports(1, &vp);
}

direct3d::direct3d(HWND window_handle)
    : _window_handle(window_handle)
    , _backbuffer_width(0)
    , _backbuffer_height(0)
    , _aspect_ratio(0.0f)
    , _driver_type(D3D_DRIVER_TYPE_HARDWARE)
    , _feature_level(D3D_FEATURE_LEVEL_11_0)
    , _device(nullptr)
    , _device_context(nullptr)
    , _swap_chain(nullptr)
    , _render_target_view(nullptr)
    , _debug(nullptr)
    , _info_queue(nullptr)
{
    _initialize_direct3d();
}

direct3d::~direct3d() {
    if(_device_context) _device_context->ClearState();

    if(_debug) _debug->Release();
    if(_info_queue) _info_queue->Release();
    if(_render_target_view) _render_target_view->Release();
    if(_swap_chain) _swap_chain->Release();
    if(_device_context) _device_context->Release();
    if(_device) _device->Release();
}

std::shared_ptr<ID3DBlob> direct3d::compile_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model) {
    DWORD shader_flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(_DEBUG)
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    shader_flags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* compiled_shader = nullptr;
    ID3DBlob* error_blob = nullptr;
    HRESULT hr = D3DCompileFromFile(filename.c_str(), nullptr, nullptr, entry_point.c_str(), shader_model.c_str(), shader_flags, 0, &compiled_shader, &error_blob);
    if(FAILED(hr)) {
        if(error_blob != nullptr) {
            std::string error_message = static_cast<char*>(error_blob->GetBufferPointer());
            error_blob->Release();
            throw D3DEXCEPTION_D(error_message.c_str(), hr, _device);
        }
        throw D3DEXCEPTION_D("Failed to compile shader (No error blob).", hr, _device);
    }
    if(error_blob) error_blob->Release();

    return make_com_pointer(compiled_shader);
}

std::shared_ptr<ID3D11VertexShader> direct3d::compile_vertex_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model, std::shared_ptr<ID3DBlob>& shader_blob) {
    shader_blob = compile_shader_from_file(filename, entry_point, shader_model);
    ID3D11VertexShader* vertex_shader = nullptr;
    HRESULT hr = _device->CreateVertexShader(shader_blob->GetBufferPointer(), shader_blob->GetBufferSize(), nullptr, &vertex_shader);
    if(FAILED(hr)) {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
        std::string error_message("Failed to create vertex shader: ");
        error_message += converter.to_bytes(filename);
        throw D3DEXCEPTION_D(error_message.c_str(), hr, _device);
    }
    return make_com_pointer(vertex_shader);
}

std::shared_ptr<ID3D11VertexShader> direct3d::compile_vertex_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model) {
    std::shared_ptr<ID3DBlob> shader_blob;
    return compile_vertex_shader_from_file(filename, entry_point, shader_model, shader_blob);
}

std::shared_ptr<ID3D11PixelShader> direct3d::compile_pixel_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model, std::shared_ptr<ID3DBlob>& shader_blob) {
    shader_blob = compile_shader_from_file(filename, entry_point, shader_model);
    ID3D11PixelShader* pixel_shader = nullptr;
    HRESULT hr = _device->CreatePixelShader(shader_blob->GetBufferPointer(), shader_blob->GetBufferSize(), nullptr, &pixel_shader);
    if(FAILED(hr)) {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
        std::string error_message("Failed to create pixel shader: ");
        error_message += converter.to_bytes(filename);
        throw D3DEXCEPTION_D(error_message.c_str(), hr, _device);
    }
    return make_com_pointer(pixel_shader);
}

std::shared_ptr<ID3D11PixelShader> direct3d::compile_pixel_shader_from_file(const std::wstring& filename, const std::string& entry_point, const std::string& shader_model) {
    std::shared_ptr<ID3DBlob> shader_blob;
    return compile_pixel_shader_from_file(filename, entry_point, shader_model, shader_blob);
}

float direct3d::get_aspect_ratio() const {
    return _aspect_ratio;
}

ID3D11Device* direct3d::get_device() {
    return _device;
}

ID3D11DeviceContext* direct3d::get_device_context() {
    return _device_context;
}

IDXGISwapChain* direct3d::get_swap_chain() {
    return _swap_chain;
}

ID3D11RenderTargetView* direct3d::get_render_target_view() {
    return _render_target_view;
}
