#pragma once

#include "beeper.hpp"
#include "direct3d.hpp"
#include "display.hpp"
#include "input.hpp"
#include <memory>
#include "chip8_system.hpp"
#include "timer.hpp"
#include "wasapi.hpp"
#include "window.hpp"
#include <Windows.h>

class emulator {
private:
    std::unique_ptr<window>       _window;
    std::shared_ptr<direct3d>     _d3d;
    std::shared_ptr<input>        _input;
    std::unique_ptr<chip8_system> _chip8;
    std::shared_ptr<display>      _display;
    std::unique_ptr<timer>        _frame_timer;
    std::unique_ptr<timer>        _instruction_timer;
    std::shared_ptr<wasapi>       _wasapi;
    std::unique_ptr<beeper>       _beeper;

    uint32_t                      _instructions_per_second;
    double                        _min_seconds_per_instruction;
    double                        _seconds_per_instruction;
    double                        _seconds_per_title_update;
    uint32_t                      _instructions_last_frame;
    uint32_t                      _instructions_since_last_title_update;

    std::string                   _path_to_program;

    void _set_custom_wnd_proc();
    void _build_and_attach_menu();
    void _tweak_seconds_per_instruction(const double dt);
    void _load_program_from_file(const std::string& path);
    void _load_placeholder_program();
    void _update_window_title(const double dt);

public:
    emulator(HINSTANCE instance_handle);

    void run();
};