'''Chip 8 Execution Path Grapher
Usage:
    graph_path.py graph <filepath>

Options:
    -h --help   Show this screen.
'''

import sys
import re
from docopt import docopt
from graphviz import Digraph


def get_file_contents(filepath):
    try:
        with open(filepath, 'r') as file:
            return file.read()
    except (OSError, IOError):
        print('Failed to open file: {}'.format(filepath))
        sys.exit()


def render_graph_from_log(log):
    graph = Digraph(name='Execution Path', format='png', engine='dot')
    graph.attr('graph', label='Execution Path', labelloc='t')
    last = '0x200'
    has_node = {last: True}
    for pass_number, counter in enumerate(re.finditer(r'Opcode ([0-9A-F]+).*\n.*\n.+?PC:([0-9A-F]+)', log)):
        opcode = hex(int(counter.group(1), 16))
        pc = hex(int(counter.group(2), 16) - 2)
        name = opcode + ', ' + pc
        if pc not in has_node:
            has_node[name] = True
            if opcode[2] == 'd':
                graph.node(name, name, style='filled', fillcolor='red')
            else:
                graph.node(name, name)
        graph.edge(last, name, str(pass_number))
        last = name
    try:
        print('Rendering graph')
        graph.render('execution_path', view=True)
    except RuntimeError as err:
        print('Render failed:', err)
        sys.exit()


def main(args):
    log = get_file_contents(args['<filepath>'])
    render_graph_from_log(log)

main(docopt(__doc__))
