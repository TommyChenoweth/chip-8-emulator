Texture2D display_texture : register(t0);
SamplerState display_sampler : register(s0);

struct VS_INPUT {
    float4 pos : POSITION;
    float2 tex : TEXCOORD0;
};

struct PS_INPUT {
    float4 pos : SV_POSITION;
    float2 tex : TEXCOORD0;
};

PS_INPUT VS(VS_INPUT input)
{
    PS_INPUT output = (PS_INPUT)0;
    output.pos = input.pos;
    output.tex = input.tex;
    return output;
}

float4 PS(PS_INPUT input) : SV_Target
{
    return display_texture.Sample(display_sampler, input.tex);
}
